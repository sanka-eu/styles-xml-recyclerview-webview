package com.example.fragmentsfragmentslifecyclenavigation.ui.login

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentLoginBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class LoginFragment : Fragment() {
    private lateinit var viewModel: LoginViewModel
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = LoginFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val editLoginEmailAddress = binding.editLoginEmailAddress
        val editLoginPassword = binding.editLoginPassword
        val toSignUpButton = binding.toSignUpButton
        val loginButton  = binding.loginButton

        viewModel.email.observe(viewLifecycleOwner) {
            editLoginEmailAddress.setText(it)
        }

        viewModel.password.observe(viewLifecycleOwner) {
            editLoginPassword.setText(it)
        }

        toSignUpButton.setOnClickListener {
            findNavController().navigate(R.id.action_login_to_registration)
        }

        loginButton.setOnClickListener {
            when (true) {
                editLoginEmailAddress.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_email),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editLoginPassword.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_password),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                else -> {
                    findNavController().navigate(R.id.action_login_to_home)
                    requireActivity()
                        .findViewById<BottomNavigationView>(R.id.nav_view)
                        .visibility = View.VISIBLE
                }
            }
        }


        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}