package com.example.fragmentsfragmentslifecyclenavigation.ui.web

import androidx.appcompat.app.AppCompatActivity
import com.example.fragmentsfragmentslifecyclenavigation.R

class BackClass : AppCompatActivity() {
    override fun onBackPressed() {
        val fragment =
            this.supportFragmentManager.findFragmentById(R.id.container)
        (fragment as? BackInterface)?.onBackPressed()?.not()?.let {
            super.onBackPressed()
        }
    }
}