package com.example.fragmentsfragmentslifecyclenavigation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.fragmentsfragmentslifecyclenavigation.databinding.ActivityMainBinding
import com.example.fragmentsfragmentslifecyclenavigation.ui.home.HomeFragment
import com.example.fragmentsfragmentslifecyclenavigation.ui.login.LoginFragment
import com.example.fragmentsfragmentslifecyclenavigation.ui.web.WebFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        navView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.navigation_home -> openFragment(HomeFragment.newInstance())
                R.id.navigation_web -> openFragment(WebFragment.newInstance())
                else -> openFragment(LoginFragment.newInstance())
            }

            return@setOnItemSelectedListener true
        }
        navView.visibility = View.GONE
    }

    private fun openFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.nav_host_fragment_activity_main, fragment)
            .commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
            //additional code
        } else {
            supportFragmentManager.popBackStack()
        }
    }
}