package com.example.fragmentsfragmentslifecyclenavigation.ui.web

interface BackInterface {
    fun onBackPressed(): Boolean
}