package com.example.fragmentsfragmentslifecyclenavigation.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentRegistrationBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class RegistrationFragment : Fragment() {
    private lateinit var viewModel: RegistrationViewModel
    private var _binding: FragmentRegistrationBinding? = null
    private val binding get() = _binding!!


    companion object {
        fun newInstance() = RegistrationFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[RegistrationViewModel::class.java]
        _binding = FragmentRegistrationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val editSignUpEmailAddress = binding.editSignUpEmailAddress
        val editSignUpPassword = binding.editSignUpPassword
        val toSignUpButton = binding.toLoginButton
        val loginButton  = binding.signUpButton

        viewModel.email.observe(viewLifecycleOwner) {
            editSignUpEmailAddress.setText(it)
        }

        viewModel.password.observe(viewLifecycleOwner) {
            editSignUpPassword.setText(it)
        }

        toSignUpButton.setOnClickListener {
            findNavController().navigate(R.id.action_registration_to_login)
        }

        loginButton.setOnClickListener {
            when (true) {
                editSignUpEmailAddress.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_email),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editSignUpPassword.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_password),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                else -> {
                    findNavController().navigate(R.id.action_registration_to_home)
                    requireActivity()
                        .findViewById<BottomNavigationView>(R.id.nav_view)
                        .visibility = View.VISIBLE
                }
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}